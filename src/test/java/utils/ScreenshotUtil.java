package utils;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

import java.io.File;
import java.io.IOException;

public class ScreenshotUtil {

    public static void captureScreenshots(WebDriver driver,String screenShotName)  {

        TakesScreenshot ts = (TakesScreenshot)driver;
        File source = ts.getScreenshotAs(OutputType.FILE);
        try {
            FileUtils.copyFile(source,new File("./Screenshots/"+screenShotName+".png"));
        } catch (Exception e) {
           System.out.print("Exception while taking screenshot" + e.getMessage());
        }
    }
}
