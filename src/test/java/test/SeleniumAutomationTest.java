package test;


import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import utils.ExcelUtils;
import utils.ScreenshotUtil;

import java.util.concurrent.TimeUnit;

import static junit.framework.TestCase.assertTrue;

public class SeleniumAutomationTest {

    private ExcelUtils excelUtils;
    private String[][] data = null;
    private String fileName;
    public static WebDriver driver = null;

    private String name;
    private String email;
    private String phone;


    public SeleniumAutomationTest(){

    }

    @Before
    public void setup() throws Exception{

        //set properties for web driver
        System.setProperty("webdriver.chrome.driver","C:\\Users\\Command Quality\\Documents\\chromedriver.exe");
        //instantiate drivers
        driver = new ChromeDriver();
       // driver = new FirefoxDriver();
        driver.manage().window().maximize();
        driver.get("https://www.ilabquality.com/");

        excelUtils = new ExcelUtils();
        fileName = "src/test/resources/TestData.xlsx";
    }

    @Test
    public void test() throws Exception{

        data = excelUtils.readExcelDataFileToArray(fileName, "PersonalDetails");
        String[] headers = ExcelUtils.getColumHeaders(data);


        for(int i = 1; i < data.length; i++){

            name = ExcelUtils.getCellValue(data[i], headers, "Name");
            email = ExcelUtils.getCellValue(data[i], headers, "Email");
            phone = ExcelUtils.getCellValue(data[i], headers, "Phone");
        }

        //click careers tab
        driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
        WebElement careers= driver.findElement(By.xpath("/html/body/header/div/div/div[3]/nav/ul/li[4]/a"));
        careers.click();
        //screenshot for careers
        ScreenshotUtil.captureScreenshots(driver,"careers");


        //select on country tab
        driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
        WebElement country= driver.findElement(By.cssSelector("div.vc_btn3-container:nth-child(9)"));
        country.click();
        //screenshot for country tab
        ScreenshotUtil.captureScreenshots(driver,"country selected");

        //click on first available job
        driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
        WebElement openPositions=driver.findElement(By.cssSelector("div.wpjb-grid-row:nth-child(1) > div:nth-child(2) > span:nth-child(1) > a:nth-child(1)"));
        openPositions.click();
        //screenshot for jobs available
        ScreenshotUtil.captureScreenshots(driver,"available job selected");

        //click on Apply online button
        driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
        WebElement applyOnline=driver.findElement(By.cssSelector(".wpjb-form-toggle"));
        applyOnline.click();
        //screenshot for Apply Online button
        ScreenshotUtil.captureScreenshots(driver,"apply online button");

        //enter applicant name
        driver.manage().timeouts().implicitlyWait(200, TimeUnit.SECONDS);
        WebElement userName=driver.findElement(By.id("applicant_name"));
        userName.sendKeys(name);
        //screenshot for entering applicant name
        ScreenshotUtil.captureScreenshots(driver,"applicant name");

        //enter applicant email
        WebElement emailAdress=driver.findElement(By.id("email"));
        emailAdress.sendKeys(email);
        //screenshot for entering email
        ScreenshotUtil.captureScreenshots(driver,"email");

        //enter applicant phone number
        WebElement phoneNumber=driver.findElement(By.id("phone"));
        phoneNumber.sendKeys(phone);
        //screenshot for entering phone number
        ScreenshotUtil.captureScreenshots(driver,"phone");

        //click on send application
        WebElement sendApplicationButton=driver.findElement(By.cssSelector("#wpjb_submit"));
        sendApplicationButton.click();
        //screenshot for send application
        ScreenshotUtil.captureScreenshots(driver,"send application");

        //validate error message if displayed
        boolean isTheTextDisplayed=driver.getPageSource().contains("You need to upload at least one file.");
        assertTrue(isTheTextDisplayed);
        //screenshot for validation message
        ScreenshotUtil.captureScreenshots(driver,"error message");

    }

    @After
    public void endTest(){
       driver.close();
    }

}
